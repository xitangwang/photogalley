package com.xtwroot.android.photogallery.exception;


/********************************************************************************
 * using for: 一个异常的构造方法
 * 丁酉鸡年正月 2017/02/22 11:35
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 *  xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public class OkHttpException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * the server return code
     */
    private int ecode;

    /**
     * the server return error message
     */
    private Object emsg;

    public OkHttpException(int ecode,Object emsg){
        this.ecode = ecode;
        this.emsg = emsg;
    }

    public int getEcode() { return ecode; }

    public Object getEmsg() { return emsg; }

}
