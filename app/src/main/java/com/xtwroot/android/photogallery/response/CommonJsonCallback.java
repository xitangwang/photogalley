package com.xtwroot.android.photogallery.response;

import android.os.Handler;
import android.os.Looper;

import com.xtwroot.android.photogallery.exception.OkHttpException;
import com.xtwroot.android.photogallery.listener.DisposeDataHandle;
import com.xtwroot.android.photogallery.listener.DisposeDataListener;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/********************************************************************************
 * using for: the logic layer exception,may alter in different app
 * 丁酉鸡年正月 2017/02/22 17:51
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 * xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public class CommonJsonCallback implements Callback {

    protected final String REQUEST_CODE = "ecode";
    protected final int REQUEST_CODE_VALUE = 0;
    protected final String ERROR_MSG = "emsg";
    protected final String EMPTY_MSG = "";
    // can has the value of set-cookie2

    /**
     * the java layer exception, do not same to the logic error
     */
    protected final int NETWORK_ERROR = -1;
    protected final int JSON_ERROR = -2;
    protected final int OTHER_ERROR = -3;

    private DisposeDataListener mListener;
    private Class<?> mCalss;
    private Handler mDelieverHandler;

    public CommonJsonCallback(DisposeDataHandle handle) {
        mListener = handle.mListener;
        mCalss = handle.mClass;
        mDelieverHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onFailure(Call call, final IOException e) {
        mDelieverHandler.post(new Runnable() {
            @Override
            public void run() {
                mListener.onFailure(e);
            }
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        final String result = response.body().string();
        mDelieverHandler.post(new Runnable() {
            @Override
            public void run() {
                handleResponse(result);
            }
        });

    }

    private void handleResponse(String result)
    {
        if(result == null || result.equals(""))
        {
            mListener.onFailure(new OkHttpException(NETWORK_ERROR,EMPTY_MSG));
            return;
        }

        try{
            JSONObject resultObj = new JSONObject(result);
            if(resultObj.has(REQUEST_CODE))
            {
                if(resultObj.optInt(REQUEST_CODE) == REQUEST_CODE_VALUE)
                {
                    if(mCalss == null)
                    {
                        mListener.onSuccess(resultObj);
                    }
                    else
                    {

                        // TODO: 解析JSON的代码
                        Object obj = null;

                        if(obj == null)
                        {
                            mListener.onFailure(new OkHttpException(JSON_ERROR,EMPTY_MSG));
                        }
                        else
                        {
                            mListener.onSuccess(obj);
                        }

                    }
                }
                else
                {
                    mListener.onFailure(new OkHttpException(JSON_ERROR,EMPTY_MSG));
                }
            }
        }
        catch (Exception e)
        {
            mListener.onFailure(new OkHttpException(OTHER_ERROR,e.getMessage()));
        }
    }
}
