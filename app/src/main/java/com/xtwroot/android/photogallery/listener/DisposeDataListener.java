package com.xtwroot.android.photogallery.listener;

/********************************************************************************
 * using for:
 * 丁酉鸡年正月 2017/02/22 12:17
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 * xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public interface DisposeDataListener {

    public void onSuccess(Object responseObj);

    public void onFailure(Object reasonObj);

}
