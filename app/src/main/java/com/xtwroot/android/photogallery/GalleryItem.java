package com.xtwroot.android.photogallery;

import android.net.Uri;

/********************************************************************************
 * using for:
 * 丁酉鸡年正月 2017/02/23 15:24
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 * xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public class GalleryItem
{
    private String mCaption;
    private String mId;
    private String mUrl;
    private String mOwner;

    public Uri getOwner() {
        return Uri.parse(mOwner);
    }

    public void setOwner(String owner) {
        mOwner = owner;
    }

    public String getCaption() {
        return mCaption;
    }

    public void setCaption(String caption) {
        mCaption = caption;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public String toString() {
        return mCaption;

    }
}
