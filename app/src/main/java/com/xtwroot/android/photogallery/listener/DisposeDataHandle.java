package com.xtwroot.android.photogallery.listener;

/********************************************************************************
 * using for: 处理真正的响应
 * 丁酉鸡年正月 2017/02/22 12:15
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 * xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public class DisposeDataHandle {

    public DisposeDataListener mListener = null;
    public Class<?> mClass = null;

    public DisposeDataHandle(DisposeDataListener listener)
    {

        mListener = listener;
    }

    public DisposeDataHandle(DisposeDataListener listener,Class<?> clazz)
    {

        mListener = listener;
        mClass = clazz;
    }

}
