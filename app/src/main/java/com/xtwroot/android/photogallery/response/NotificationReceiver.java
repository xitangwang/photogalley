package com.xtwroot.android.photogallery.response;

import android.app.Activity;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.xtwroot.android.photogallery.PollService;

/********************************************************************************
 * using for:
 * 丁酉鸡年二月 2017/03/04 15:49
 *
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 *         xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public class NotificationReceiver extends BroadcastReceiver {

    private static final String TAG = "NotificationReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive: received result: " + getResultCode());
        if(getResultCode() != Activity.RESULT_OK){
            // A foreground activity cancelled the broadcast
            return;
        }
        int requestCode = intent.getIntExtra(PollService.RESULT_CODE,0);
        Notification notification = (Notification) intent.getParcelableExtra(PollService.NOTIFICATION);

        NotificationManagerCompat notificationManagerCompat =
                NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(requestCode,notification);
    }
}
