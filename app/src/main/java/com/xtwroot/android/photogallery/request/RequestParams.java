package com.xtwroot.android.photogallery.request;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/********************************************************************************
 * using for:
 * 丁酉鸡年正月 2017/02/22 12:22
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 * xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public class RequestParams {

    public ConcurrentHashMap<String,String> urlParams = new ConcurrentHashMap<>();
    public ConcurrentHashMap<String,Object> fileParams = new ConcurrentHashMap<>();


    public RequestParams(Map<String,String> source){
        if(source != null){
            for(Map.Entry<String,String> entry : source.entrySet()){
                put(entry.getKey(),entry.getValue());
            }
        }
    }

    public RequestParams()
    {

    }


    public RequestParams put(String key,String value){
        urlParams.put(key,value);
        return this;
    }

}
