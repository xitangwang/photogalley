package com.xtwroot.android.photogallery;

import com.lzy.okgo.OkGo;
import com.xtwroot.android.photogallery.https.HttpsUtils;
import com.xtwroot.android.photogallery.listener.DisposeDataHandle;
import com.xtwroot.android.photogallery.response.CommonJsonCallback;

import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Call;
import okhttp3.Request;

/********************************************************************************
 * using for:
 * 丁酉鸡年正月 2017/02/22 21:20
 * @author 西唐王, xtwyzh@gmail.com,xtwroot.com
 * xtwroot Copyrights (c) 2017. All rights reserved.
 ********************************************************************************/
public class OkHttpClient
{

    private static final int TIME_OUT = 30;
    private static okhttp3.OkHttpClient mOkHttpClient;

    static
    {
        okhttp3.OkHttpClient.Builder okHttpBuilder = new okhttp3.OkHttpClient.Builder();
        okHttpBuilder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        okHttpBuilder.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
        okHttpBuilder.readTimeout(TIME_OUT,TimeUnit.SECONDS);
        okHttpBuilder.writeTimeout(TIME_OUT,TimeUnit.SECONDS);
        okHttpBuilder.followRedirects(true);
        //okHttpBuilder.sslSocketFactory(HttpsUtils.getSslSocketFactory());
        //okhttp3.OkHttpClient okGo = new OkGo().getOkHttpClient();

        mOkHttpClient = okHttpBuilder.build();
    }

    public static void post(Request request, DisposeDataHandle handle)
    {
        Call call = mOkHttpClient.newCall(request);

        call.enqueue(new CommonJsonCallback(handle));

    }

    public static void get(Request request,DisposeDataHandle handle)
    {
        Call call = mOkHttpClient.newCall(request);

        call.enqueue(new CommonJsonCallback(handle));
    }
}
